#version 330

layout (location = 0) in vec3 pos; // Location = 0, Input value, 3 axes, Variable name
layout (location = 1) in vec2 tex;
layout (location = 2) in vec3 norm;

out vec4 vCol;
out vec2 TexCoord;
out vec3 Normal;
out vec3 FragPos;
out vec4 DirectionalLightSpacePos;

uniform mat4 model;
uniform mat4 projection;
uniform mat4 view;
uniform mat4 directionalLightTransform;

void main()
{
	gl_Position = projection * view * model * vec4(pos, 1.0);	// pos = pos.x,pos.y,pos,z Output position which has 4 axes
	DirectionalLightSpacePos = directionalLightTransform * model * vec4(pos, 1.0);
	
	vCol = vec4(clamp(pos, 0.0f, 1.0f), 1.0f); // Colorare in functie de poztie
	
	TexCoord = tex;

	Normal = mat3(transpose(inverse(model))) * norm; // Normals are moving with the object (translation/scaling/rotation)
	
	FragPos = (model * vec4(pos, 1.0)).xyz; // GLSL feature to transform vec4 in vec3
}