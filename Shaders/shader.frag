#version 330

in vec4 vCol;
in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPos;
in vec4 DirectionalLightSpacePos;

out vec4 colour; // Just a single output

const int MAX_POINT_LIGHTS = 3;
const int MAX_SPOT_LIGHTS = 3;

struct Light
{
	vec3 colour;
	float ambientIntensity;
	float diffuseIntensity;
};

struct DirectionalLight 
{
	Light base;
	vec3 direction;
};

struct PointLight
{
	Light base;
	vec3 position;
	float constant;
	float linear;
	float exponent;
};

struct SpotLight
{
	PointLight base;
	vec3 direction;
	float edge;
};

struct OmniShadowMap
{
	samplerCube shadowMap;
	float farPlane;
};


struct Material
{
	float specularIntensity;
	float shininess;
};

uniform int pointLightCount;
uniform int spotLightCount;

uniform DirectionalLight directionalLight;
uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform SpotLight spotLights[MAX_SPOT_LIGHTS];

uniform sampler2D theTexture; // Texture Unit 0
uniform sampler2D directionalShadowMap; // Texture Unit 1
uniform OmniShadowMap omniShadowMaps[MAX_POINT_LIGHTS + MAX_SPOT_LIGHTS];

uniform Material material;

uniform vec3 eyePosition; // Camera position

vec3 sampleOffsetDirections[20] = vec3[]
(
	// R (Right), L (Left), D (Down), U (Up), B (Back),  F (Front)
	//   R, U,  B		  R,  D,  B 		L,  D,  B,		  L, U,  B
	vec3(1, 1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1, 1,  1), 
	vec3(1, 1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
	vec3(1, 1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1, 1,  0),
	vec3(1, 0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1, 0, -1),
	vec3(0, 1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0, 1, -1)
); // 20 calculations for 1 Pixel

float CalcShadowFactor(vec4 DirectionalLightSpacePos)
{
	vec3 projCoords = DirectionalLightSpacePos.xyz / DirectionalLightSpacePos.w; // Relative position of the light source
	projCoords = (projCoords * 0.5) + 0.5; // Change the result values from -1 - 1 to 0-1
	
	float closestDepth = texture(directionalShadowMap, projCoords.xy).r;
	float current = projCoords.z; // How far is from the light
	
	vec3 normal = normalize(Normal);
	vec3 lightDir = normalize(directionalLight.direction);
	
	// (Change 0.05)
	float bias = max(0.05 * (1- dot(normal, lightDir)), 0.005); // Calculate the bias
	
	float shadow = 0.0;
	
	vec2 texelSize = 1.0 / textureSize(directionalShadowMap, 0);
	for(int x = -1; x <= 1; ++x)
	{
		for(int y = -1; y <= 1; ++y)
		{
			float pcfDepth = texture(directionalShadowMap, projCoords.xy + vec2(x, y) * texelSize).r;  // Closest depth
			shadow += current - bias > pcfDepth ? 1.0 : 0.0; // 1 (Full Shadow) 0 (No Shadow)
		}
	}
	
	shadow /= 9.0; // Percentage-Closer Filtering (PCF)
	
	if(projCoords.z > 1.0)
	{
		shadow = 0.0;
	}
	
	return shadow;
}

float CalcPointShadowFactor(PointLight light, int shadowIndex)
{
	vec3 fragToLight = FragPos - light.position;
	float currentDepth = length(fragToLight);
	
	float shadow = 0.0;
	float bias = 0.15;
	int samples = 20;
	//float diskRadius = 0.05;
	
	float viewDistance = length(eyePosition - FragPos); // Distance between Camera and Fragment
	float diskRadius = (1.0 + (viewDistance/omniShadowMaps[shadowIndex].farPlane)) / 25.0;
	// diskRadius is calculated based on how far away the camera is from the pixel, so make it more blurred the closer we get to it
	
	/* TOO MANY CALCULATIONS FOR 1 PIXEL
	float samples = 4.0;
	float offset = 0.1;
	for(float x = -offset; x < offset; x += offset / (samples * 0.5))
	{
		for(float y = -offset; y < offset; y += offset / (samples * 0.5))
		{
			for(float z = -offset; z < offset; z += offset / (samples * 0.5))
			{
				float closestDepth = texture(omniShadowMaps[shadowIndex].shadowMap, fragToLight + sampleOffsetDirections[i] * diskRadius).r;
				closestDepth *= omniShadowMaps[shadowIndex].farPlane;
				if(currentDepth - bias > closestDepth)
				{
					shadow += 1.0;
				}
			}
		}
	} // 64 calculations for 1 Pixel
	shadow /= (samples * samples * samples);
	*/
	
	// Alternative solution: 20 calculations for 1 Pixel
	for(int i = 0; i < samples; i++)
	{
		float closestDepth = texture(omniShadowMaps[shadowIndex].shadowMap, fragToLight + sampleOffsetDirections[i] * diskRadius).r;
		closestDepth *= omniShadowMaps[shadowIndex].farPlane;
		if(currentDepth - bias > closestDepth)
		{
			shadow += 1.0;
		}
	}
	
	shadow /= float(samples);
	return shadow;
}

vec4 CalcLightByDirection(Light light, vec3 direction, float shadowFactor)
{
	vec4 ambientColour = vec4(light.colour, 1.0f) * light.ambientIntensity;

	float diffuseFactor = max(dot(normalize(Normal), normalize(direction)), 0.0f); // normalize(...) -> Converting to unit vector
	//ex: A . B = |A| * |B| * cos(angle) // normalize(...) // A . B = 1*1*cos(angle) = cos(angle) = [0..1]
	vec4 diffuseColour = vec4(light.colour * light.diffuseIntensity * diffuseFactor, 1.0f);

	vec4 specularColour = vec4(0, 0, 0, 0);
	
	// If diffuse light exists, then apply specular light
	// If an area is not being hit by deffuse lighting, then there's not going to be any specular lighting
	if(diffuseFactor > 0.0f)
	{
		vec3 fragToEye = normalize(eyePosition - FragPos); // Direction of the Fragment from the Eye
		vec3 reflectedVertex = normalize(reflect(direction, normalize(Normal))); // Direction the light is being reflected in 
		// Reflect the 'direction' to the 'Normal'
		
		float specularFactor = dot(fragToEye, reflectedVertex);
		if(specularFactor > 0.0f)
		{
			specularFactor = pow(specularFactor, material.shininess);
			specularColour = vec4(light.colour * material.specularIntensity * specularFactor, 1.0f);
		}
	}
	
	return (ambientColour + (1.0 - shadowFactor) * (diffuseColour + specularColour)); // With A&D&S*Shadow Light
}

vec4 CalcDirectionalLight(vec4 DirectionalLightSpacePos)
{
	float shadowFactor = CalcShadowFactor(DirectionalLightSpacePos);
	
	return CalcLightByDirection(directionalLight.base, directionalLight.direction, shadowFactor);
}

vec4 CalcPointLight(PointLight pLight, int shadowIndex)
{
	vec3 direction = FragPos - pLight.position;
	float distance = length(direction);
	direction = normalize(direction);
	
	float shadowFactor = CalcPointShadowFactor(pLight, shadowIndex);
	
	vec4 colour = CalcLightByDirection(pLight.base, direction, shadowFactor);
	// Attenuation Factor
	float attenuation = pLight.exponent * distance * distance +
						pLight.linear * distance +
						pLight.constant;
		
	return (colour / attenuation); // Colour resulted
}

vec4 CalcSpotLight(SpotLight sLight, int shadowIndex)
{
	vec3 rayDirection = normalize(FragPos - sLight.base.position); // Direction of Fragment to Spot Light
	float slFactor = dot(rayDirection, sLight.direction);
	
	if(slFactor > sLight.edge)
	{
		vec4 colour = CalcPointLight(sLight.base, shadowIndex);
		
		return colour * (1.0f - (1.0f - slFactor)*(1.0f/(1.0f - sLight.edge)));
	} else {
		return vec4(0, 0, 0, 0); // Applying no light
	}
}

vec4 CalcPointLights()
{
	vec4 totalColour = vec4(0, 0, 0, 0);
	for(int i = 0; i < pointLightCount; i++)
	{
		totalColour += CalcPointLight(pointLights[i], i); // Combine colours
	}
	
	return totalColour;
}

vec4 CalcSpotLights()
{
	vec4 totalColour = vec4(0, 0, 0, 0);
	for(int i = 0; i < spotLightCount; i++)
	{
		totalColour += CalcSpotLight(spotLights[i], i + pointLightCount); // Combine colours
	}
	
	return totalColour;
}

float CalcFogFactor()
{
	// (Change) Fog Intensity at distance
	float fogDensity = 0.05f;
	float fragmentDistance = length(eyePosition - FragPos);

	//float fogFactor = (fogEnd - fragmentDistance)/(fogEnd - fogStart); // Linear Fog
	//float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2)); // Exponential Fog
	float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2)); // Exponential Squared Fog
	
	return clamp(fogFactor, 0.0f, 1.0f); // Constrain a value to lie between two further values (minValue, maxValue)
}

void main()
{	
	vec4 finalColour = CalcDirectionalLight(DirectionalLightSpacePos);
	finalColour += CalcPointLights(); // Add Point Lights
	finalColour += CalcSpotLights(); // Add Spot Lights
	
	//colour = texture(theTexture, TexCoord) * finalColour;
	vec4 sceneColour = texture(theTexture, TexCoord) * finalColour; // Find TexCoord to match it to theTexture
	
	if(sceneColour.a < 0.1) // If Alpha Color is smaller than 0.1 then don't display the colour of the pixel (Transparent)
		discard;
	
	// Adding Fog
    float fogFactor = CalcFogFactor();
	// (Change) Fog Color
	vec4 fogColor = vec4(0.2f, 0.2f, 0.2f, 1.0f); // Colour
	colour = mix(fogColor, sceneColour, fogFactor); // Linearly interpolate between the colour of the scene and the fog factor
	colour = fogColor*(1-fogFactor) + sceneColour * fogFactor; // Final Colour
	
	//colour = vec4(1.0f, 0.0f, 0.0f, 1.0f);	// RGB (x,y,z)
	//colour = vCol;
	//colour = texture(theTexture, TexCoord); // Find TexCoord to match it to theTexture
	//colour = texture(theTexture, TexCoord) * vCol; // With RGB Colour
	//colour = texture(theTexture, TexCoord) * ambientColour; // With A Light
	//colour = texture(theTexture, TexCoord) * (ambientColour + diffuseColour); // With A&D Light
}