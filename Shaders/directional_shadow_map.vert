#version 330
// Where thing are in relation to a light (Won't appear on the screen)

layout (location = 0) in vec3 pos;

uniform mat4 model;
uniform mat4 directionalLightTransform;


void main()
{
	gl_Position = directionalLightTransform * model * vec4(pos, 1.0);
}