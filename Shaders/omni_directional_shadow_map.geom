#version 330

layout (triangles) in;
layout (triangle_strip, max_vertices=18) out; // 3 x 6 sides of the cube

uniform mat4 lightMatrices[6];

out vec4 FragPos;

void main()
{
	for(int face = 0; face < 6; face++)
	{
		gl_Layer = face; // Which of the 6 layers to draw
		for(int i = 0; i < 3; i++)
		{
			FragPos = gl_in[i].gl_Position; // Take the 3 points of the triangle
			gl_Position = lightMatrices[face] * FragPos;
			EmitVertex(); // Creates vertex at location stored in gl_Position
		}
		EndPrimitive(); // Stores primitive created by last EmitVertex() calls, and starts a new primitive
	}
}
